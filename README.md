CSCI344-Project-1
=================

Elements for the design borrowed from Sypris Solutions and Fix8. Specifically, header
design was borrowed from Sypris while column divisions were borrowed from Fix8.

US Capitol Building image licensed via Creative Commons. 
Bird designed by Jennifer Gamboa from the Noun Project.
Conversation image designed by Dave Tappy, public domain.
E-mail image designed by Douglas Nash, public domain.
Like image designed by Marwa Boukarim, public domain.
